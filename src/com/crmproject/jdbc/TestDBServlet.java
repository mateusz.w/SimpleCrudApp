package com.crmproject.jdbc;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/testDBconnection")
public class TestDBServlet extends HttpServlet
{
	private static final long serialVersionUID = 1620868715343109193L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String jdbcUrl = "jdbc:mysql://localhost:3306/web_customer_tracker?useSSL=false&serverTimezone=UTC";
		String userName = "hbstudent";
		String password = "hbstudent";
		
		try(Connection connection = DriverManager.getConnection(jdbcUrl, userName, password))
		{
			response.getWriter().print("Connection was succesful");
			System.out.println("Connection was succesful");
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
}
