package com.crmproject.dao;

import java.util.List;

import com.crmproject.entity.Customer;

public interface CustomerDAO
{
	public void saveCustomer(Customer customer);

	public Customer getCustomer(Integer id);

	public List<Customer> getCustomers();

	public void updateCustomer(Customer customer);

	public void deleteCustomer(Integer id);

	public void testMethod();
}
