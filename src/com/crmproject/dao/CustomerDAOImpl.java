package com.crmproject.dao;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import com.crmproject.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO
{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Resource
	ApplicationContext context;
	
	public void testMethod()
	{
		System.out.println(getCustomers().toString());
		Arrays.stream(context.getBeanDefinitionNames()).forEach(s -> System.out.println(s));
	}
	
	public CustomerDAOImpl()
	{
		System.out.println("in CustomerDAOImpl constructor");
	}
	
	public List<Customer> getCustomers()
	{
		return sessionFactory.getCurrentSession().createQuery("from Customer", Customer.class).getResultList();
	}
	
	public void saveCustomer(Customer customer)
	{
		sessionFactory.getCurrentSession().saveOrUpdate(customer);
	}
	
	public Customer getCustomer(Integer id)
	{
		return sessionFactory.getCurrentSession().get(Customer.class, id);
	}
	
	public void updateCustomer(Customer customer)
	{
		sessionFactory.getCurrentSession().update(customer);
	}
	
	public void deleteCustomer(Integer id)
	{
		sessionFactory.getCurrentSession()
					  .createQuery("delete from Customer where id like :customerId")
					  .setParameter("customerId", id)
					  .executeUpdate();
	}

}
