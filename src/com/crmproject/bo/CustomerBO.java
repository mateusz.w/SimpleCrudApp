package com.crmproject.bo;

import java.util.List;

import com.crmproject.dao.CustomerDAO;
import com.crmproject.entity.Customer;

public interface CustomerBO
{
	public void saveCustomer(Customer customer);
	public Customer getCustomer(Integer id);
	public List<Customer> getCustomers();
	public void updateCustomer(Customer customer);
	public void deleteCustomer(Integer id);
}
