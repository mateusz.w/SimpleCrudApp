package com.crmproject.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crmproject.dao.CustomerDAO;
import com.crmproject.entity.Customer;

@Service
public class CustomerBOImpl implements CustomerBO
{
	
	@Autowired
	private CustomerDAO customerDAO;

	@Override
	@Transactional
	public void saveCustomer(Customer customer)
	{
		customerDAO.saveCustomer(customer);
	}

	@Override
	@Transactional
	public Customer getCustomer(Integer id)
	{
		return customerDAO.getCustomer(id);
	}

	@Override
	@Transactional
	public List<Customer> getCustomers()
	{
		//sort((c1, c2) -> c1.getFirstName().compareTo(c2.getFirstName())
		List<Customer> customers = customerDAO.getCustomers();
		customers.sort((c1, c2) -> c1.getLastName().compareTo(c2.getLastName()));
		return customers;
	}

	@Override
	@Transactional
	public void updateCustomer(Customer customer)
	{
		customerDAO.updateCustomer(customer);
	}

	@Override
	@Transactional
	public void deleteCustomer(Integer id)
	{
		customerDAO.deleteCustomer(id);
	}
	
	
}
