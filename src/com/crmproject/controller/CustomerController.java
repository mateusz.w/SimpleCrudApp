package com.crmproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.crmproject.bo.CustomerBO;
import com.crmproject.entity.Customer;

@Controller
@RequestMapping("/customer")
public class CustomerController
{
	@Autowired
	CustomerBO customerBO;
	
	@GetMapping("/customerAddForm")
	public String showAddCustomerForm(Model model)
	{
		model.addAttribute("customer", new Customer());
		return "customer-add-form";
	}
	
	@GetMapping("/list")
	public ModelAndView listCustomers(Model model)
	{
		List<Customer> customers = customerBO.getCustomers();
		model.addAttribute("customers", customers);
		
		return new ModelAndView("list-customers", "model", model);
	}
	
	@GetMapping("/showFormForUpdate")
	public String updateCustomer(@RequestParam(name="customerId") Integer id, Model model)
	{
		model.addAttribute(customerBO.getCustomer(id));
		
		return "customer-add-form";
	}
	
	@PostMapping("/saveCustomer")
	public String saveCustomer(@ModelAttribute("customer")Customer customer)
	{
		customerBO.saveCustomer(customer);
		return "redirect:/customer/list";
	}
	
	@RequestMapping("/deleteCustomer")
	public String deleteCustomer(@RequestParam(name="customerId") Integer id)
	{
		customerBO.deleteCustomer(id);
		return "redirect:/customer/list";
	}
	
	@RequestMapping("/test")
	public void testMethod(@RequestParam("username")String userName)
	{
		customerBO.getCustomers().stream().forEach(System.out::println);
	}
	
}
